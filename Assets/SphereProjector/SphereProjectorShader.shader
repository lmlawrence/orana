﻿Shader "Custom/SphereProjectorShader" {
	Properties {
		//_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		//_Glossiness ("Smoothness", Range(0,1)) = 0.5
		//_Metallic ("Metallic", Range(0,1)) = 0.0
        _ProjectorCenter ("ProjectorCenter", Vector) = (0,0,0,0)
        _ProjectorRotation ("ProjectorRotation", Vector) = (0,0,0,1)
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		//LOD 200

		Lighting Off
		Cull Back

		CGPROGRAM
		#pragma surface surf Unlit vertex:vert
		// Physically based Standard lighting model, and enable shadows on all light types
		//#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		//#pragma target 3.0

		 fixed4 LightingUnlit(SurfaceOutput s, fixed3 lightDir, fixed atten)
	     {
	         //fixed4 c;
	         //c.rgb = s.Albedo; 
	         //c.a = s.Alpha;
	         return fixed4(0,0,0,0);
	     }

		sampler2D _MainTex;
        float4 _ProjectorCenter;
        float4 _ProjectorRotation;

		struct Input {
			//float2 uv_MainTex;
			float3 worldPos;
			float3 localPos;
            float3 worldNormal;
		};

		//half _Glossiness;
		//half _Metallic;
		//fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		//UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		//UNITY_INSTANCING_CBUFFER_END
        
        ////////////////// BEGIN QUATERNION FUNCTIONS //////////////////

        float PI = 3.1415926535897932384626433832795;
        
        float4 setAxisAngle (float3 axis, float rad) {
          rad = rad * 0.5;
          float s = sin(rad);
          return float4(s * axis[0], s * axis[1], s * axis[2], cos(rad));
        }
        
        float3 xUnitVec3 = float3(1.0, 0.0, 0.0);
        float3 yUnitVec3 = float3(0.0, 1.0, 0.0);
        
        float4 rotationTo (float3 a, float3 b) {
          float vecDot = dot(a, b);
          float3 tmpvec3 = float3(0, 0, 0);
          if (vecDot < -0.999999) {
            tmpvec3 = cross(xUnitVec3, a);
            if (length(tmpvec3) < 0.000001) {
              tmpvec3 = cross(yUnitVec3, a);
            }
            tmpvec3 = normalize(tmpvec3);
            return setAxisAngle(tmpvec3, PI);
          } else if (vecDot > 0.999999) {
            return float4(0,0,0,1);
          } else {
            tmpvec3 = cross(a, b);
            float4 _out = float4(tmpvec3[0], tmpvec3[1], tmpvec3[2], 1.0 + vecDot);
            return normalize(_out);
          }
        }
        
        float4 multQuat(float4 q1, float4 q2) {
          return float4(
            q1.w * q2.x + q1.x * q2.w + q1.z * q2.y - q1.y * q2.z,
            q1.w * q2.y + q1.y * q2.w + q1.x * q2.z - q1.z * q2.x,
            q1.w * q2.z + q1.z * q2.w + q1.y * q2.x - q1.x * q2.y,
            q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z
          );
        }
        
        float3 rotateVector( float4 quat, float3 vec ) {
          // https://twistedpairdevelopment.wordpress.com/2013/02/11/rotating-a-vector-by-a-quaternion-in-glsl/
          float4 qv = multQuat( quat, float4(vec, 0.0) );
          return multQuat( qv, float4(-quat.x, -quat.y, -quat.z, quat.w) ).xyz;
        }
        
        ////////////////// END QUATERNION FUNCTIONS //////////////////

		void vert (inout appdata_full v, out Input o) {
		   UNITY_INITIALIZE_OUTPUT(Input,o);
		   o.localPos = v.vertex.xyz;
		 }

		void surf (Input IN, inout SurfaceOutput o) {

			const float3 xaxis = float3(1,0,0);
			const float pi = 3.1415926535897932384626433832795;
			// Albedo comes from a texture tinted by color
			//fixed4 c = tex2D (_MainTex, IN.uv_MainTex);// * _Color;
			//float3 localPos = IN.worldPos -  mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;
			
            //float3 localPos = IN.localPos;
            float3 worldPos = IN.worldPos;
            
            float3 dir = worldPos - _ProjectorCenter.xyz;
            float3 shadow_n = normalize(dir);//(localPos.xyz); // this is for shadow only
            
            // rotate dir
            float4 q = float4(-_ProjectorRotation.x,
                              -_ProjectorRotation.y,
                              -_ProjectorRotation.z,
                               _ProjectorRotation.w);
            dir = dir + 2.0 * cross(q.xyz, cross(q.xyz, dir) + q.w * dir); // rotate dir with q
            //dir = rotateVector(q, dir);
            float3 n = normalize(dir);
            
			
            if(dot(shadow_n, IN.worldNormal) < 0)
            {
                float3 nxz = normalize(float3(dir.x, 0, dir.z));//(float3(localPos.x, 0, localPos.z));
                float v = ((((n.y<0)?-1:1) * acos(dot(n, nxz))/(pi/2)) + 1) / 2;
                float u = ((((nxz.z<0)?1:-1) * acos(dot(nxz, xaxis))/pi) + 1) / 2;
                //u += _ProjectorCenter.x / 10;
                float2 uv = float2(u,v);
                o.Emission = tex2D (_MainTex, uv);//IN.uv_MainTex);//c.rgb;
            }
			// Metallic and smoothness come from slider variables
			//o.Metallic = _Metallic;
			//o.Smoothness = _Glossiness;
			//o.Alpha = 1;
		}
		ENDCG

	}
	//FallBack "Unlit"
}
