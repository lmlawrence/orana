﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereProjectorBehaviour : MonoBehaviour {

    public GameObject[] targetObjects;

    public Material projectorMaterial;


	// Use this for initialization
	void Start () {
        if(!projectorMaterial)
            projectorMaterial = GetComponent<Renderer>().material;

        foreach(GameObject to in targetObjects) {
            Renderer r = to.GetComponent<Renderer>();
            if (r)
                r.material = projectorMaterial;
        }

	}

    void Update () {
    }

    // Update is called once per frame
    void LateUpdate () {
        projectorMaterial.SetVector("_ProjectorCenter", 
                                    new Vector4(transform.position.x, 
                                                transform.position.y, 
                                                transform.position.z, 1));

        projectorMaterial.SetVector("_ProjectorRotation",
                                    new Vector4(transform.rotation.x,
                                                transform.rotation.y,
                                                transform.rotation.z,
                                                transform.rotation.w));
    }
}
