﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RaycastMainMenu : MonoBehaviour {
    private OVRInput.Controller controller;
    public GameObject laserPrefab;
    private GameObject laser;

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<OVRControllerHelper>() != null) {
            controller = GetComponent<OVRControllerHelper>().m_controller;
        }
        //controllerInputSource = GetComponent<SteamVR_Behaviour_Pose>();
        laser = Instantiate(laserPrefab); // Create a new clone of the laserPrefab gameobject at run-time.
        highlightedBlock = ColorBlock.defaultColorBlock;
        highlightedBlock.normalColor = Color.yellow;
    }

    private ColorBlock highlightedBlock;
    private Button lastHighlightedToggle;

    private void highlightButton(RaycastHit hit) {
        Button toggle = hit.transform.GetComponent<Button>();
        if (lastHighlightedToggle != null) {
            lastHighlightedToggle.colors = ColorBlock.defaultColorBlock;
        }
        lastHighlightedToggle = toggle;
        toggle.colors = highlightedBlock;
    }

    void UpdateLaserTransform(RaycastHit hit) {
        // Update the position, rotation, and scale of the laser.
        if (laser != null) {
            laser.transform.position = Vector3.Lerp(this.transform.position, hit.point, 0.5f);
            laser.transform.LookAt(hit.point);
            laser.transform.localScale = new Vector3(0.01f, 0.01f, hit.distance);
        }
    }

    [SerializeField]
    private sceneManager sManager;
    public void selectMenuButton() {
        //if (SteamVR_Actions._default.InteractUI.GetStateDown(controllerInputSource.inputSource)) {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) {
            // Return to main menu.
            sManager.loadScene("MENU");
        }
    }

    // Update is called once per frame
    void Update() {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit)) {
            //Debug.Log("Ray is currently hitting:" + hit.transform.name);
            UpdateLaserTransform(hit);
            if (hit.transform.name == "MainMenuButton") {
                highlightButton(hit);
                selectMenuButton();
            } else {
                if (lastHighlightedToggle != null) {
                    lastHighlightedToggle.colors = ColorBlock.defaultColorBlock;
                }
            }
        }
    }
}
