﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dockerTask : MonoBehaviour {
    [SerializeField]
    private sceneManager sceneManage;
    public int successfulDocks = 0;
    public int failedDocks = 0;
    public int totalDocks = 0;
    public float elapsedTime = 0f;

    void Update() {
        elapsedTime += Time.deltaTime;
    }

    public void checkCompletion() {
        totalDocks++;
        Debug.Log("Successful Docks:"+ successfulDocks + ", Failed Docks:" + failedDocks + ", "  +"Total Docks = " + totalDocks);
        if (totalDocks == 9) {
            if (failedDocks <= 1) {
                participantInfo.TUTORIAL_COMPLETED = true;
            }
            sceneManage.loadScene("MENU");
        }
    }
}
