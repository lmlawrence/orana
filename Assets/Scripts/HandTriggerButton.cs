﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HandTriggerButton : MonoBehaviour
{
    [SerializeField]
    private sceneManager sManager;

    public void OnTriggerEnter(Collider other) {
        Debug.Log("Col:" + other.name);
        if (other.transform.name == "OVRControllerPrefab" || other.transform.name == "OVRHandPrefab") {
            Debug.Log("Return to menu");
            sManager.loadScene("MENU");
        }
    }
}
