﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerChecker : MonoBehaviour {
    public csvWriter writer;
    [SerializeField]
    private dockerTask docker;
    private void OnTriggerEnter(Collider other) {
        OVRGrabbable grabScript = other.GetComponent<OVRGrabbable>();
        if (docker != null && grabScript != null && !grabScript.isGrabbed && other.GetComponent<MeshRenderer>().enabled && (other.tag == "shape1" || other.tag == "shape2" || other.tag == "shape3")) {
            if (other.transform.tag == this.transform.tag) {
                docker.successfulDocks++;
                writer.WriteLine(docker.elapsedTime + "," + (docker.totalDocks+1) + "," + other.transform.name + ",TRUE");
            } else {
                writer.WriteLine(docker.elapsedTime + "," + (docker.totalDocks+1) + "," + other.transform.name + ",FALSE");
                docker.failedDocks++;
            }
            StartCoroutine(delayedDestroy(other.gameObject));
            docker.checkCompletion();
        }
    }

    private IEnumerator delayedDestroy(GameObject obj) {
        obj.GetComponent<MeshRenderer>().enabled = false;
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;
        Destroy(obj);
    }
}
