﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuProBehaviour : MonoBehaviour
{
    public SortableItemCollection items;
    public Creator creator;
    public ItemSpawner itemSpawner;
    public ProgressBar progress;

    public TMPro.TextMeshProUGUI numberText;
    public Button minusButton;
    public Button plusButton;
    public Button okayButton;

    private int val;
    private int min;
    private int max;

    // Start is called before the first frame update
    void Start()
    {
        progress.GetComponent<Canvas>().enabled = false;

        //max = items.transform.childCount;
        //max = creator.prefabs.Length;
        //max = itemSpawner.transform.childCount;
        max = itemSpawner.items.transform.childCount;
        min = 0;
        val = (max + min) / 2;
        min = 1;
        updateNumberText();
    }

   

    // Update is called once per frame
    void Update()
    {

    }

    public void confirm()
    {
        // the okay button
        
        progress.enabled = true;

        //int unused = max - val;
        //items.SetActiveFirstItems(true, val);
        //items.SetActiveLastItems(false, unused);

        itemSpawner.items.SetActiveFirstItems(true, val);
        
        //itemSpawner.SpawnItem(0);
        //creator.CreateNext();

        progress.Initialize(val);
        
        progress.GetComponent<Canvas>().enabled = true;

        //progress.UpdateSlider();
        //progress.UpdateText();

        this.GetComponent<Canvas>().enabled = false;

        //itemSpawner.RemoveNumberOfUnusedItemsRandom(unused);
        //Debug.Log("items removed = " + itemSpawner.transform.childCount);

        itemSpawner.items.CleanUpInactiveItems();
        itemSpawner.SpawnItem();
        //items.CleanUpInactiveItems();
        //Destroy(canvas);
    }

    private void updateNumberText()
    {
        numberText.text = val.ToString();
    }

    public void decrease()
    {
        // the minus button
        if (val > min)
        {
            val = val - 1;
            updateNumberText();
        }
    }

    public void increase()
    {
        // the plus button
        if (val < max)
        {
            val = val + 1;
            updateNumberText();
        }
    }
}
