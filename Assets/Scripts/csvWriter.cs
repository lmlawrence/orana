﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class csvWriter : MonoBehaviour {
    /// ===============================
    /// AUTHOR: Kieran William May
    /// PURPOSE: Writes to and generates a .csv file
    /// NOTES:
    /// (If CSV file(s) with the same name already exists it will loop through using a counter until it finds a name that doesn't exist)
    /// ===============================
    /// 

    public bool isTutorial, logData;
    private List<string> logInfo = new List<string>();

    private string FILE_NAME = "ID";
    private const string DIR = "Logs/";
    private float timeRunning = 0f;
    public void WriteLine(string line) {
        logInfo.Add(line);
        overriteExistingFile(); // Relog data each time just incase a crash or issue occurs.
        count++;
    }
    private int count = 0;
    public int getCount() {
        return count;
    }

    private void Start() {
        if (!isTutorial && participantInfo.IS_SECOND_CHANCE) {
            string oldFileName = FILE_NAME;
            FILE_NAME = "Second_Chance_" + oldFileName;
        }

        if (!isTutorial) {
            //WriteLine("Timestamp, Count, ItemName, Bin, isCorrect, sessionNo");
            WriteLine("DATE,SUBJECT_ID,SESSION_NUMBER,TIMESTAMP,ATTEMPT,ITEM_NAME,BIN,CORRECT,PERFECT_SCORE_PER_BIN,PERFECT_SCORE");
            // DATE, SUBJECT_ID, SESSION_NUMBER, TIMESTAMP, ATTEMPT, ITEM_NAME, BIN, CORRECT, PERFECT_SCORE
        } else if (isTutorial) {
            //WriteLine("Timestamp, Count, ItemName, isCorrect");
            string oldFileName = FILE_NAME;
            FILE_NAME = "Tutorial" + oldFileName;
            WriteLine("TIMESTAMP,ATTEMPT,ITEM_NAME,CORRECT");
        }
    }

    public float getTimeStamp() {
        return timeRunning;
    }

    private void Update() {
        timeRunning += Time.deltaTime;
        if (logData) {
            overriteExistingFile();
            logData = false;
        }
    }

    public void overriteExistingFile() {
        string dest = FILE_NAME + participantInfo.PARTICIPANT_ID.ToString() + "S" + participantInfo.SESSION_NO.ToString() + ".csv";
        #if !UNITY_EDITOR
        dest = System.IO.Path.Combine(Application.persistentDataPath, FILE_NAME + participantInfo.PARTICIPANT_ID.ToString() + "S" + participantInfo.SESSION_NO.ToString() + ".csv");
        #endif
        StreamWriter writer = null;
        int count = 1;
        bool fileExists = File.Exists(dest);
        if (fileExists) {
            dest = FILE_NAME + participantInfo.PARTICIPANT_ID.ToString() + "S" + participantInfo.SESSION_NO.ToString() + ".csv";
            #if !UNITY_EDITOR
            dest = System.IO.Path.Combine(Application.persistentDataPath, FILE_NAME + participantInfo.PARTICIPANT_ID.ToString() + "S" + participantInfo.SESSION_NO.ToString() + ".csv");
            #endif
            count++;
            Debug.Log(dest + " already exists - Failed to log data.");
            writer = new StreamWriter(dest, false) as StreamWriter;
            //return; // Exit for now. <- Need to override the file if it exists?
        } else {
            print("Found path:" + dest);
            writer = new StreamWriter(dest, true) as StreamWriter;
        }
        print("File exists?" + fileExists);
        for (int i = 0; i < logInfo.Count; i++) {
            writer.Write(logInfo[i]);
            writer.WriteLine();
        }
        print("Writen to file:" + dest);
        writer.Close();
    }

}