﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class revertKinematicObjects : MonoBehaviour {
    [SerializeField]
    private Transform parentTransform;
    private void OnCollisionEnter(Collision col) {
        //if (col.transform.tag == "waste" || col.transform.tag == "organic" || col.transform.tag == "recycle" || col.transform.tag == "shape1" || col.transform.tag == "shape2" || col.transform.tag == "shape3") {
        if ((col.transform.parent == null || col.transform.parent == parentTransform) && (col.transform.tag == "waste" || col.transform.tag == "organic" || col.transform.tag == "recycle" || col.transform.tag == "shape1" || col.transform.tag == "shape2" || col.transform.tag == "shape3")) {
            Rigidbody rb = col.gameObject.GetComponent<Rigidbody>();
            if (rb != null) {
                rb.isKinematic = true;
            }
        }
    }

}
