﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creator : MonoBehaviour
{
    public GameObject[] prefabs;

    private AudioSource feedbackSound;

    // Start is called before the first frame update
    void Start()
    {
        feedbackSound = gameObject.GetComponent<AudioSource>();
        //CreateRandom();        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Create(int index)
    {
        Debug.Log("create: " + index);
        Bounds bounds = prefabs[index].GetComponent<Collider>().bounds;
        
        Instantiate(prefabs[index], transform.position - new Vector3(0, prefabs[index].transform.position.y - bounds.min.y, 0), Quaternion.identity);
        feedbackSound.Play();
    }

    private int currIndex = 0;
    public void CreateNext()
    {
        Create(currIndex);
        currIndex = (currIndex + 1) % prefabs.Length;
    }

    public void CreateRandom()
    {
        currIndex = Random.Range(0, prefabs.Length);
        Create(currIndex);
    }

}
