﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class sceneManager : MonoBehaviour {
    [SerializeField]
    private bool loadSceneInEditor = false;
    public void loadScene(string name) {
        if (name == "MENU") {
            SceneManager.LoadScene(0);
        } else if (name == "TUTORIAL") {
            SceneManager.LoadScene(1); // Don't have a tutorial scene yet?
        } else if (name == "MAIN") {
            participantInfo.IS_SECOND_CHANCE = false;
            SceneManager.LoadScene(2);
        } else if (name == "MAIN_SECOND_CHANCE") {
            participantInfo.IS_SECOND_CHANCE = true;
            SceneManager.LoadScene(2);
        } else if (name == "FINISH") {
            SceneManager.LoadScene(3);
        }
    }

}
