﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortableItem : MonoBehaviour
{
    //Transform originalTransform;
    Vector3 originalPosition;
    Quaternion originalRotation;
    Material originalMaterial;
    Collider collider;
    Rigidbody rigidbody;

    //ItemSpawner itemSpawner;
    int numberOfAttempts = 0;

    public void UpdateNumberOfAttempts() {
        numberOfAttempts = numberOfAttempts + 1;
    }

    public void ResetMaterial() {
        this.GetComponent<Renderer>().material = originalMaterial;
    }

    public int GetNumberOfAttempts() {
        return numberOfAttempts;
    }

    public void ResetTransform() {
        this.transform.position = new Vector3(originalPosition.x, originalPosition.y, originalPosition.z);
        this.transform.rotation = new Quaternion(originalRotation.x, originalRotation.y, originalRotation.z, originalRotation.w);
    }

    public void SetTransformNow() {
        SetPositionNow();
        SetRotationNow();
        //originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        // = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
    }

    public void SetTransformAtPosition(Vector3 newPosition) {
        //Debug.Log(collider);
        originalPosition = new Vector3(newPosition.x, newPosition.y + collider.bounds.extents.y, newPosition.z);
    }

    public void SetPositionNow() {
        originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    public void SetRotationNow() {
        originalRotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
    }

    public void FreezeRotation(bool val) {
        rigidbody.freezeRotation = val;
    }

    // Start is called before the first frame update
    void Start()
    {
        SetTransformNow();
        originalMaterial = GetComponent<Renderer>().material;
        collider = gameObject.GetComponent<Collider>();
        //if (collider == null)
        //{
        //    Debug.Log(name + " has no collider");
        //}
        //else {
        //    Debug.Log(name + " has collider");
        //}

        //Debug.Log(collider);
        rigidbody = gameObject.GetComponent<Rigidbody>();
        //Debug.Log(this.name);
        rigidbody.freezeRotation = true;
        //Debug.Log(this.name + " = " + collider.bounds.size);

        //itemSpawner = GetComponentInParent<ItemSpawner>();


        //ItemSpawner itemSpawner = transform.GetComponentInParent<ItemSpawner>();
        //if (itemSpawner != null) {
        //    // this means that we need to remove the rigid body
        //    Rigidbody rigidbody = transform.GetComponent<Rigidbody>();
        //    if (rigidbody != null) {
        //        Destroy(rigidbody);
        //    }
        //}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
