﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericGrabber : OVRGrabber
{
    private OVRHand mHand;
    private float pinchThreshold = 0.65f;
    private float releaseThreshold = 0.55f;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        mHand = GetComponent<OVRHand>();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        CheckIndexPinch();
    }

    void CheckIndexPinch()
    {
        float pinchStrength = mHand.GetFingerPinchStrength(OVRHand.HandFinger.Index);

        if (!m_grabbedObj && pinchStrength > pinchThreshold && m_grabCandidates.Count > 0)
        {
            GrabBegin();
        }
        else if (m_grabbedObj && pinchStrength < releaseThreshold)
        {
            GrabEnd();
        }
    }
}
