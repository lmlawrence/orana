﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortableItemCollection : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        //HideAllItems();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowAllItems() {
        foreach (Transform t in transform) {
            t.gameObject.SetActive(true);
        }
    }

    public void HideAllItems() {
        foreach (Transform t in transform) {
            t.gameObject.SetActive(false);
        }
    }

    void SetActiveItemAtIndex(bool val, int index) {
        transform.GetChild(index).gameObject.SetActive(val);
    }

    public void SetActiveFirstItem(bool val)
    {
        SetActiveItemAtIndex(val, 0);
    }

    public void SetActiveLastItem(bool val)
    {
        SetActiveItemAtIndex(val, transform.childCount - 1);
    }

    public void SetActiveFirstItems(bool val, int num)
    {
        for (int i = 0; i < num; i++) {
            SetActiveItemAtIndex(val,i);
        }
    }

    public void SetActiveLastItems(bool val, int num) {
        int len = transform.childCount-1;
        for (int i = 0; i < num; i++) {
            SetActiveItemAtIndex(val,len-i);
        }
    }

    public void CleanUpInactiveItems() {
        foreach (Transform t in transform) {
            if (t.gameObject.activeSelf == false) {
                Destroy(t.gameObject);
            }
        }
    }
}
