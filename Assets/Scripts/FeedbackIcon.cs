﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackIcon : MonoBehaviour
{
    float createdAt = 0;

    // Start is called before the first frame update
    void Start()
    {
        createdAt = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time - createdAt > 2.0f)
        {
            Destroy(gameObject);
        }
    }
}
