﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class generateOptions : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Dropdown dropdown = GetComponent<Dropdown>();
        for (int i=1; i<=99; i++) {
            Dropdown.OptionData optionData = new Dropdown.OptionData();
            optionData.text = i.ToString();
            dropdown.options.Add(optionData);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
