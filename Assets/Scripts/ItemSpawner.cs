﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    private AudioSource feedbackSound;
    public SortableItemCollection items;
    public GameObject spawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        feedbackSound = gameObject.GetComponent<AudioSource>();
        //Debug.Log("spawn item count = " + transform.childCount);
        
        //foreach (Transform t in transform)
        //{
        //    //t.transform.position = transform.position - transform.right;
        //    //Rigidbody rb = t.GetComponentInChildren<Rigidbody>();
        //    //if (rb != null)
        //    //{
        //    //    rb.freezeRotation = true;
        //    //    //rb.isKinematic = true;
        //    //}
        //    t.gameObject.SetActive(false);
        //}
    }

    public void SpawnItem(int index) {
        //GameObject itemToClone = transform.GetChild(index).gameObject;
        //itemToClone.SetActive(true);
        //GameObject spawnedItem = Instantiate(itemToClone);
        //itemToClone.SetActive(false);
        //spawnedItem.transform.position = this.transform.position;
        //spawnedItem.AddComponent<SortableItem>();

        GameObject item = items.transform.GetChild(index).gameObject;
        //item.AddComponent<SortableItem>();

        //item.SetActive(true);
        SortableItem itemSortable = item.GetComponent<SortableItem>();
        //itemSortable.SetTransformAtPosition(this.transform.position);

        itemSortable.SetRotationNow();
        itemSortable.SetTransformAtPosition(spawnPoint.transform.position);
        itemSortable.ResetTransform();
        itemSortable.FreezeRotation(false);
        //item.transform.parent = transform;

        //item.GetComponent<SortableItem>().ResetTransform();
        //Rigidbody rb = item.GetComponentInChildren<Rigidbody>();
        //if (rb != null)
        //{
        //    rb.freezeRotation = false;
        //    //rb.isKinematic = false;
        //}

        feedbackSound.Play();
    }

    public void SpawnItem() {
        //int index = Random.Range(0, items.transform.childCount);
        int index = 0;
        SpawnItem(index);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
