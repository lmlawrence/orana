﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RecycleChecker : MonoBehaviour
{
    public int type;
    [SerializeField]
    private csvWriter writer;
    public GameObject correctIconPrefab;
    public GameObject wrongIconPrefab;
    public AudioSource correctSound;
    public AudioSource wrongSound;
    public ProgressBar progress;

    //private Creator creator;

    private int noOfSorted = 0;
    private int noOfPerfect = 0;

    void IncrementPerfect() {
        noOfPerfect = noOfPerfect + 1;
    }

    void IncrementSorted() {
        noOfSorted = noOfSorted + 1;
    }

    public int GetNoOfPerfect() {
        return noOfPerfect;
    }

    public int GetNoOfSorted() {
        return noOfSorted;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Testing logs
        Debug.Log("ID=" + participantInfo.PARTICIPANT_ID + " SESSION=" + participantInfo.SESSION_NO + " Tut Completed?=" + participantInfo.TUTORIAL_COMPLETED);
        //creator = FindObjectOfType<Creator>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    [SerializeField]
    private TMPro.TextMeshProUGUI perfectText;
    private void OnTriggerEnter(Collider other)
    {
        if (tag.Equals(other.gameObject.tag))
        {
            var go = Instantiate(correctIconPrefab, transform.position + new Vector3(0, 0.8f, 0), Quaternion.identity);
            go.GetComponent<Rigidbody>().AddForce(new Vector3(0, 15f, 0));
            correctSound.Play();

            IncrementSorted();
            SortableItem item = other.gameObject.GetComponent<SortableItem>();
            if (item.GetNumberOfAttempts() == 0) {
                IncrementPerfect();
            }

            // write to file
            string name = item.name;
            string bin = tag;
            string accuracy = "YES";
            string timeStamp = writer.getTimeStamp().ToString();

            //string lineToWrite = timeStamp + "," + writer.getCount().ToString() + "," + name + "," + bin + ","+accuracy + "," + participantInfo.SESSION_NO.ToString();
            //int perfectScoreToInt = int.Parse(perfectText.text) + 1;
            int perfectScore = progress.CountPerfectFromRecycleCheckers();
            string lineToWrite = System.DateTime.Now.ToString() + "," + participantInfo.PARTICIPANT_ID.ToString() + "," + participantInfo.SESSION_NO.ToString() + "," + timeStamp + "," + writer.getCount().ToString() + "," + name.Trim().ToLower() + "," + bin + "," + accuracy + "," + GetNoOfPerfect().ToString() + "," + perfectScore;
            // DATE, SUBJECT_ID, SESSION_NUMBER, TIMESTAMP, ATTEMPT, ITEM_NAME, BIN, CORRECT, PERFECT_SCORE

            writer.WriteLine(lineToWrite);            
            DestroyObject(other);
        }
        else
        {
            var go = Instantiate(wrongIconPrefab, transform.position + new Vector3(0, 0.8f, 0), Quaternion.identity);
            go.GetComponent<Rigidbody>().AddForce(new Vector3(0, 15f, 0));
            wrongSound.Play();

            //other.gameObject.GetComponent<SortableItem>().ResetTransform();
            SortableItem item = other.gameObject.GetComponent<SortableItem>();
            item.UpdateNumberOfAttempts();
            item.ResetTransform();
            progress.errorsMade++;
            // write to file
            string name = item.name;
            if (!participantInfo.failedItemsInSession.Contains(name)) {
                Debug.Log("Adding:" + other.gameObject);
                participantInfo.failedItemsInSession.Add(name);
            }
            Debug.Log("Failed items size:" + participantInfo.failedItemsInSession.Count);
            string bin = tag;
            string accuracy = "NO";
            string timeStamp = writer.getTimeStamp().ToString();

            //string lineToWrite = timeStamp + "," + writer.getCount().ToString() + "," + name + "," + bin + "," + accuracy + "," + participantInfo.SESSION_NO.ToString();
            //int perfectScoreToInt = int.Parse(perfectText.text) + 1;
            int perfectScore = progress.CountPerfectFromRecycleCheckers();
            string lineToWrite = System.DateTime.Now.ToString() + "," + participantInfo.PARTICIPANT_ID.ToString() + "," + participantInfo.SESSION_NO.ToString() + "," + timeStamp + "," + writer.getCount().ToString() + "," + name.Trim().ToLower() + "," + bin + "," + accuracy + "," + GetNoOfPerfect().ToString() + "," + perfectScore;
            writer.WriteLine(lineToWrite);
            
        }

    }

    private void DestroyObject(Collider other) {
        if (other.gameObject.tag.Equals("recycle") || other.gameObject.tag.Equals("waste") || other.gameObject.tag.Equals("container") || other.gameObject.tag.Equals("green") || other.gameObject.tag.Equals("organic"))
            Destroy(other.gameObject);
    }

    private void TriggerNext()
    {
        //creator.CreateNext();
    }
}
