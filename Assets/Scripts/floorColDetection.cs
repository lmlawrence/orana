﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floorColDetection : MonoBehaviour {

    void OnCollisionEnter(Collision col) {
        if (col.transform.tag == "waste" || col.transform.tag == "organic" || col.transform.tag == "recycle" || col.transform.tag == "shape1" || col.transform.tag == "shape2" || col.transform.tag == "shape3") {
            // Reset back to table if user drops an item.
            SortableItem item = col.gameObject.GetComponent<SortableItem>();
            item.ResetTransform();
        }
    }

}
