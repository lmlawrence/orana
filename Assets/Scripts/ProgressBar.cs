﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public TMPro.TextMeshProUGUI progressText;
    public Slider slider;
    public TMPro.TextMeshProUGUI perfectText;

    public Creator creator;
    public SortableItemCollection items;
    public ItemSpawner itemSpawner;
    public RecycleChecker[] recycleCheckers;

    public AudioSource applauseSound;

    [SerializeField]
    private int sorted;
    private int total;
    //private int unsorted;
    private int perfect;

    private int CountSortedFromRecycleCheckers() {
        int count = 0;
        foreach (RecycleChecker r in recycleCheckers) {
            count = count + r.GetNoOfSorted();
        }
        return count;
    }

    public int CountPerfectFromRecycleCheckers() {
        int count = 0;
        foreach (RecycleChecker r in recycleCheckers)
        {
            count = count + r.GetNoOfPerfect();
        }
        return count;
    }
    
    public void Initialize(int num) {
        perfect = 0;
        sorted = 0;
        total = num;

        //DELETE OR COMMENT THIS - FOR TESTING PURPOSES ONLY
        /*if (participantInfo.IS_SECOND_CHANCE) {
            total = num;
        } else {
            total = 1;
        }*/
        Debug.Log("Total:"+total);

        //unsorted = num;

        slider.minValue = sorted;
        slider.maxValue = total;
        slider.value = sorted;

        UpdateSlider();
        UpdateText();

    }
    [SerializeField]
    private sceneManager sceneManager;
    private bool loadingNewScene = false;
    public int errorsMade = 0;
    public void UpdateProgress() {
        int count = CountSortedFromRecycleCheckers();
        if (count > sorted) {
            // means that there has been a change
            // so changes have to be made
            // the slider changes
            // and it is okay to create a new item

            perfect = CountPerfectFromRecycleCheckers(); ;

            //unsorted = unsorted - (recycleCount - sorted);
            sorted = count;
            //total = sorted + unsorted;

            //total = itemSpawner.items.transform.childCount + sorted;
            //unsorted = total - sorted;
            //total = itemSpawner.transform.childCount + sorted;
            //total = creator.prefabs.Length;

            UpdateSlider();
            UpdateText();

            if (IsCompleted() && !loadingNewScene) {
                loadingNewScene = true;
                CompletionText();
                applauseSound.Play();
                // Based on the mistakes made back to menu or to completion scene..
                if (!participantInfo.IS_SECOND_CHANCE && errorsMade == 0) {
                    participantInfo.ERRORS_MADE_PREV_ROUND = errorsMade;
                }
                if (participantInfo.IS_SECOND_CHANCE || errorsMade == 0) {
                    if (participantInfo.ERRORS_MADE_PREV_ROUND <= 2 || participantInfo.SESSION_NO > 20) {
                        participantInfo.SESSION_NO++;
                        sceneManager.loadScene("FINISH");
                    } else {
                        participantInfo.SESSION_NO++;
                        sceneManager.loadScene("MENU");
                    }
                } else if (!participantInfo.IS_SECOND_CHANCE && errorsMade >= 1) {
                    participantInfo.ERRORS_MADE_PREV_ROUND = errorsMade;
                    sceneManager.loadScene("MAIN_SECOND_CHANCE");
                }
            }
            else
            {
                //itemSpawner.SpawnItem();
                //creator.CreateNext();
            }

            //else {
            //    int diff = total - sorted;
            //    if (diff >= 2) {
            //        //creator.CreateNext();
            //        itemSpawner.SpawnItem();
            //    }
            //}
        }
    }

    public void UpdateSlider() {
        slider.value = sorted;
        //slider.onValueChanged.Invoke(sorted);
    }

    public void UpdateText() {
        progressText.text = sorted.ToString() + "/" + total.ToString();
        perfectText.text = perfect.ToString();
    }

    public bool IsCompleted() {
        if (sorted >= total) {
            return true;
        }
        else {
            return false;
        }
        
    }

    public void CompletionText() {
        if (IsCompleted()) {
            // TODO celebrate
            //this.enabled = false;
            progressText.text = progressText.text + "\nFINISH";
        }
    }

    // Start is called before the first frame update
    void Start() {
        Debug.Log("Launched trash scene - second chance:" + participantInfo.IS_SECOND_CHANCE);
        if (participantInfo.IS_SECOND_CHANCE) {
            foreach (Transform item in items.transform) {
                if (!participantInfo.failedItemsInSession.Contains(item.name)) {
                    Destroy(item.gameObject);
                }
            }
        }
        if (participantInfo.IS_SECOND_CHANCE) {
            this.Initialize(participantInfo.failedItemsInSession.Count);
        } else {
            this.Initialize(items.transform.childCount);
        }

        participantInfo.failedItemsInSession = new List<string>();
        //itemSpawner.SpawnItem();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Errors made" + errorsMade);
        UpdateProgress();
    }
}
