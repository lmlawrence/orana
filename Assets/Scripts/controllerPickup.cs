﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controllerPickup : MonoBehaviour {

    private OVRInput.Controller controller;
    private bool objGrabbed = false;
    [SerializeField]
    private GameObject currentlySelectedObj = null;
    private Transform prevParent = null;
    [SerializeField]
    private bool controllerDown = false; // For testing in editor without vr..

    private void OnTriggerStay(Collider col) {
        if (col.transform.tag == "waste" || col.transform.tag == "organic" || col.transform.tag == "recycle" || col.transform.tag == "shape1" || col.transform.tag == "shape2" || col.transform.tag == "shape3") {
            if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger) || controllerDown) {
                //controllerDown = false;
                if (!objGrabbed && currentlySelectedObj == null) {
                    //Pickup
                    currentlySelectedObj = col.gameObject;
                    currentlySelectedObj.GetComponent<SortableItem>().ResetMaterial();
                    currentlySelectedObj.GetComponent<Rigidbody>().isKinematic = true;
                    prevParent = currentlySelectedObj.transform.parent;
                    currentlySelectedObj.gameObject.transform.SetParent(this.transform);
                    objGrabbed = true;
                }
            } else if ((OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger) || !controllerDown) &&  objGrabbed) {
            currentlySelectedObj.gameObject.transform.SetParent(prevParent);
            currentlySelectedObj.GetComponent<Rigidbody>().isKinematic = false;
            currentlySelectedObj = null;
            objGrabbed = false;
            }
        }
    }
    private bool returnToMenu = false;
    private float returnToMenuTimer = 0f;
    [SerializeField]
    private sceneManager sManager;
    private void returnToMainMenu() {
        if (sManager != null) {
            if (OVRInput.Get(OVRInput.Button.One) && OVRInput.Get(OVRInput.Button.Two) && OVRInput.Get(OVRInput.Button.Three) && OVRInput.Get(OVRInput.Button.Four)) {
                returnToMenuTimer += Time.deltaTime;
                if (returnToMenuTimer >= 2f && returnToMenu == false) {
                    sManager.loadScene("MENU");
                    returnToMenu = true;
                }
            } else {
                returnToMenuTimer = 0f;
            }
        }
    }

    [SerializeField]
    private Material highlightedMat;
    private void OnTriggerEnter(Collider col) {
        //Hovered
        if ((col.transform.tag == "waste" || col.transform.tag == "organic" || col.transform.tag == "recycle" || col.transform.tag == "shape1" || col.transform.tag == "shape2" || col.transform.tag == "shape3") && col.gameObject != currentlySelectedObj) {
            col.GetComponent<Renderer>().material = highlightedMat;
        }
    }

    private void OnTriggerExit(Collider col) {
        //Unhovered
        if (col.transform.tag == "waste" || col.transform.tag == "organic" || col.transform.tag == "recycle" || col.transform.tag == "shape1" || col.transform.tag == "shape2" || col.transform.tag == "shape3") {
            col.GetComponent<SortableItem>().ResetMaterial();
        }
    }

    // Start is called before the first frame update
    void Start() {
        controller = GetComponent<OVRControllerHelper>().m_controller;
        if (controller == OVRInput.Controller.LTrackedRemote && participantInfo.CONTROLLER_SET == participantInfo.CONTROLLER_TYPES.RIGHT) {
            this.gameObject.SetActive(false);
        } else if (controller == OVRInput.Controller.RTrackedRemote && participantInfo.CONTROLLER_SET == participantInfo.CONTROLLER_TYPES.LEFT) {
            this.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //returnToMainMenu();
    }
}
