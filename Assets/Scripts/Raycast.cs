﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR;
using UnityEngine.UI;
public class Raycast : MonoBehaviour
{
    //private SteamVR_Behaviour_Pose controllerInputSource;
    private OVRInput.Controller controller;
    private bool pickedUp = false;
    public GameObject laserPrefab;
    private GameObject laser;
    void UpdateLaserTransform(RaycastHit hit) {
        // Update the position, rotation, and scale of the laser.
        if (laser != null) {
            laser.transform.position = Vector3.Lerp(this.transform.position, hit.point, 0.5f);
            laser.transform.LookAt(hit.point);
            laser.transform.localScale = new Vector3(0.01f, 0.01f, hit.distance);
        }
    }

    private void initControllerToggles() {
        if (participantInfo.CONTROLLER_SET == participantInfo.CONTROLLER_TYPES.BOTH) {
            controllerButtons[0].isOn = true;
        } else if (participantInfo.CONTROLLER_SET == participantInfo.CONTROLLER_TYPES.RIGHT) {
            controllerButtons[1].isOn = true;
        } else if (participantInfo.CONTROLLER_SET == participantInfo.CONTROLLER_TYPES.LEFT) {
            controllerButtons[2].isOn = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<OVRControllerHelper>() != null) {
            controller = GetComponent<OVRControllerHelper>().m_controller;
        }
        //controllerInputSource = GetComponent<SteamVR_Behaviour_Pose>();
        laser = Instantiate(laserPrefab); // Create a new clone of the laserPrefab gameobject at run-time.
        highlightedBlock = ColorBlock.defaultColorBlock;
        highlightedBlock.normalColor = Color.yellow;
        if (participantInfo.PARTICIPANT_ID != null && participantInfo.PARTICIPANT_ID != "") {
            dropdownLabel.text = participantInfo.PARTICIPANT_ID.ToString(); // Participant ID already set reassigning label.
        }
        initControllerToggles();
    }

    [SerializeField]
    private Scrollbar scrollbar, scrollbar2;
    private Scrollbar activeScrollbar = null;
    private bool scrollBarIsOpen = false;

    private void scrollDropdown(OVRInput.Axis2D axis) {
        if (activeScrollbar == null) {
            return;
        }
        if (scrollBarIsOpen) { // Change to true.
            //Vector2 primaryAxis = SteamVR_Actions._default.Touchpad.GetAxis(controllerInputSource.inputSource);
            Vector2 primaryAxis = OVRInput.Get(axis);
            float absVal = Mathf.Abs(primaryAxis.y) / 20;
            if (primaryAxis.y <= -0.1) {
                float val = activeScrollbar.GetComponent<Scrollbar>().value - absVal;
                val = Mathf.Clamp(val, 0, 1);
                activeScrollbar.GetComponent<Scrollbar>().value = val;
            } if (primaryAxis.y >= 0.1) {
                float val = activeScrollbar.GetComponent<Scrollbar>().value + absVal;
                val = Mathf.Clamp(val, 0, 1);
                activeScrollbar.GetComponent<Scrollbar>().value = val;
            }
        }
    }

    public void startButton() {
        //if (SteamVR_Actions._default.InteractUI.GetStateDown(controllerInputSource.inputSource)) {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) {
      //if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger)) {
            if (selectedScene != null) {
                sceneManagement.loadScene(selectedScene);
            }
        }
    }
    [SerializeField]
    private sceneManager sceneManagement;
    public string selectedScene = "";
    [SerializeField]
    private Toggle tutorialToggle, mainToggle;
    public void changeToggle(RaycastHit hit, string name) {
        //if (SteamVR_Actions._default.InteractUI.GetStateDown(controllerInputSource.inputSource)) {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) || Input.anyKeyDown) {
        //if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger)) {
            hit.transform.parent.GetComponent<Toggle>().isOn = !hit.transform.parent.GetComponent<Toggle>().isOn;
            if (hit.transform.parent.GetComponent<Toggle>().isOn) {
                if (name == "BackgroundB") {
                    controllerButtons[1].isOn = false;
                    controllerButtons[2].isOn = false;
                    participantInfo.CONTROLLER_SET = participantInfo.CONTROLLER_TYPES.BOTH;
                } else if (name == "BackgroundR") {
                    controllerButtons[0].isOn = false;
                    controllerButtons[2].isOn = false;
                    participantInfo.CONTROLLER_SET = participantInfo.CONTROLLER_TYPES.RIGHT;
                } else if (name == "BackgroundL") {
                    controllerButtons[0].isOn = false;
                    controllerButtons[1].isOn = false;
                    participantInfo.CONTROLLER_SET = participantInfo.CONTROLLER_TYPES.LEFT;
                }

                if (name == "BackgroundMain") {
                    tutorialToggle.isOn = false;
                    selectedScene = "MAIN";
                } else if (name == "BackgroundTutorial") {
                    mainToggle.isOn = false;
                    selectedScene = "TUTORIAL";
                }
            }
        }
    }
    [SerializeField]
    private GameObject dropdownList, dropdownList2;
    private GameObject lastSelectedDropdownList;
    [SerializeField]
    private Text dropdownLabel, newDropdownLabel;
    [SerializeField]
    private Toggle[] controllerButtons; // BOTH, RIGHT, LEFT
    public void openDropdownMenu(string dropdownLabel) {
        GameObject dropdown = dropdownLabel == "dropdownLabel" ? dropdownList : dropdownList2;
        activeScrollbar = dropdownLabel == "dropdownLabel" ? scrollbar : scrollbar2;
        lastSelectedDropdownList = dropdown;
        GameObject dropdownOther = dropdownLabel == "dropdownLabel" ? dropdownList2 : dropdownList;
        //if (SteamVR_Actions._default.InteractUI.GetStateDown(controllerInputSource.inputSource)) {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) || Input.anyKeyDown) {
            dropdown.SetActive(!dropdown.activeInHierarchy);
            if (dropdownOther.activeInHierarchy) {
                dropdownOther.SetActive(false);
            }
            if (dropdown.activeInHierarchy) {
                scrollBarIsOpen = true;
            }
        }
    }

    private Toggle lastToggle;
    private ColorBlock highlightedBlock;
    private Toggle lastHighlightedToggle;
    private void highlightDropdownToggle(RaycastHit hit) {
        Toggle toggle = hit.transform.GetComponent<Toggle>();
        if (lastHighlightedToggle != null) {
            lastHighlightedToggle.colors = ColorBlock.defaultColorBlock;
        }
        lastHighlightedToggle = toggle;
        toggle.colors = highlightedBlock;
    }
    public void changeDropdownToggle(RaycastHit hit) {
        //if (SteamVR_Actions._default.InteractUI.GetStateDown(controllerInputSource.inputSource)) {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) || Input.anyKeyDown) {
        //if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger)) {
            if (lastToggle != null) {
                lastToggle.isOn = false;
            }
            lastToggle = hit.transform.GetComponent<Toggle>();
            lastToggle.isOn = true;
            newDropdownLabel.text = lastToggle.GetComponentInChildren<Text>().text;
            if (newDropdownLabel.name == "dropdownLabel") {
                participantInfo.PARTICIPANT_ID = newDropdownLabel.text;
            } else if (newDropdownLabel.name == "dropdownLabel2") {
                participantInfo.SESSION_NO = int.Parse(newDropdownLabel.text);
            }
            lastSelectedDropdownList.SetActive(false);
            scrollBarIsOpen = false;
        }
    }
        // Update is called once per frame
    void Update() {
        scrollDropdown(OVRInput.Axis2D.PrimaryThumbstick);
        scrollDropdown(OVRInput.Axis2D.SecondaryThumbstick);
        // Cast a ray from the users controller, in a foward direction.
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit)) {
            //Debug.Log("Ray is currently hitting:" + hit.transform.name);
            UpdateLaserTransform(hit);
            if (hit.transform.name.StartsWith("Background") || hit.transform.name.StartsWith("Toggle")) {
                changeToggle(hit, hit.transform.name);
            } if (hit.transform.name == "dropdownLabel" || hit.transform.name == "dropdownLabel2") {
                newDropdownLabel = hit.transform.GetComponent<Text>();
                openDropdownMenu(hit.transform.name);
            } if (hit.transform.name.StartsWith("Item")) {
                changeDropdownToggle(hit);
                highlightDropdownToggle(hit);
            } if (hit.transform.name == "StartButton") {
                startButton();
            }
        }
    }
}