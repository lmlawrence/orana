﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataFile
{
    public static void WriteToFile(string lineToWrite) {
        string path = Application.persistentDataPath + "./Data/test.txt";

        // check if the file exists
        // if not, create it
        if (!File.Exists(path)) {
            File.Create(path);
        }

        // open the file
        StreamWriter writer = new StreamWriter(path, true);
        
        // add new line to the file
        writer.WriteLine(lineToWrite);
        
        // close the file
        writer.Close();
    }
}
