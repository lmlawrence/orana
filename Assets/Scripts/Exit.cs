﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{

    [SerializeField]
    private sceneManager sceneManager;

    public void ExitFunction() {
        sceneManager.loadScene("MENU");
        //Application.Quit();
    }
}
