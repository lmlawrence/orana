﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuBehaviour : MonoBehaviour
{
    public Creator creator;
    public GameObject[] existingObjects;

    public Canvas canvas;

    public Text numberText;
    public Button minusButton;
    public Button plusButton;
    public Button okayButton;

    private int val = 10;
    private const int MIN = 5;
    private const int MAX = 20;

    public void confirm() {
        // the okay button

        canvas.enabled = false;
        //Destroy(canvas);
    }

    private void updateNumberText() {
        numberText.text = val.ToString();
    }

    private void printValues() {
        Debug.Log("val: " + val.ToString() + "\tmin: " + MIN.ToString() + "\tmax: " + MAX.ToString());
    }

    public void decrease() {
        // the minus button
        //Debug.Log("decrease");

        if (val > MIN) {
            val = val - 1;
            updateNumberText();
        }

        //printValues();

        //if (val > MAX) {
        //    Debug.Log("enabling +");
        //    val = MAX;
        //    plusButton.enabled = true;
        //}

        //val = val - 1;
        //updateNumberText();

        //if (val < MIN) {
        //    Debug.Log("disabling -");
        //    val = MIN;
        //    minusButton.enabled = false;
        //}

        //printValues();

    }

    public void increase() {
        // the plus button
        //Debug.Log("increase");

        if (val < MAX) {
            val = val + 1;
            updateNumberText();
        }

        //printValues();

        //if (val < MIN) {
        //    Debug.Log("enable -");
        //    val = MIN;
        //    minusButton.enabled = true;
        //}

        //val = val + 1;

        //updateNumberText();

        //if (val >= MAX) {
        //    Debug.Log("diable +");
        //    val = MAX;
        //    plusButton.enabled = false;
        //}

        //printValues();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
