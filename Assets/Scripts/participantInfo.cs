﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class participantInfo {
    public static string PARTICIPANT_ID = "";
    public static CONTROLLER_TYPES CONTROLLER_SET = CONTROLLER_TYPES.BOTH; //By default set to both..
    public enum CONTROLLER_TYPES {BOTH, LEFT, RIGHT};
    public static int SESSION_NO;
    public static bool TUTORIAL_COMPLETED;
    public static bool IS_SECOND_CHANCE = false;
    public static int ERRORS_MADE_PREV_ROUND;

    [System.NonSerialized]
    public static List<string> failedItemsInSession = new List<string>();
    public static List<string> getFailedItems() { return failedItemsInSession; }

    public static void loopThroughFailedItems() {
        for (int i=0; i<getFailedItems().Count; i++) {
            Debug.Log(i + " , " + failedItemsInSession[i]);
        }
    }
}
