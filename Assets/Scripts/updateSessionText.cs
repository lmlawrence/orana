﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class updateSessionText : MonoBehaviour {
    private Text label;
    [SerializeField]
    private Text sessionDropdownLabel;
    private void updateText() {
        //label.text = "Session: " + participantInfo.SESSION_NO.ToString() + "\nTutorial\nComplete=" + participantInfo.TUTORIAL_COMPLETED;
        label.text = "Session:" + "\nTutorial\nComplete=" + participantInfo.TUTORIAL_COMPLETED;
        if (participantInfo.PARTICIPANT_ID != null || participantInfo.PARTICIPANT_ID == "") {
            sessionDropdownLabel.text = participantInfo.SESSION_NO.ToString();
        }
    }

    void Start() {
        label = GetComponent<Text>();
        updateText();
    }
}
